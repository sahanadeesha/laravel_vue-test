<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/employee_store', 'App\Http\Controllers\EmployeesController@store');
Route::get('/employee_list', 'App\Http\Controllers\EmployeesController@index');
Route::delete('/employee_delete/{id}', 'App\Http\Controllers\EmployeesController@trash');
Route::put('/employee_update/{id}', 'App\Http\Controllers\EmployeesController@update');
Route::get('/employee_edit/{id}', 'App\Http\Controllers\EmployeesController@edit');
// Route::post('/user_list', function (Request $request) {
//     // localhost:8000/api/user_list 
//     return response()->json([
//         'fname' => 'adhessha',
//         'lname' => 'sahan',
//     ]);
// });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'App\Http\Controllers\Auth\LoginController@login');
Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@create');
// Route::post('/register', 'App\Http\Controllers\Auth\RegisterController@login');
