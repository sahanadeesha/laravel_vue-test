import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App.vue'

import Router from './routes.js'
import DataTable from 'laravel-vue-datatable'
import VueSweetalert2 from 'vue-sweetalert2';
import Swal from 'sweetalert2'

// import VeeValidate from 'vee-validate';

import 'sweetalert2/dist/sweetalert2.min.css';

window.axios=require('axios');
window.Swal=require('sweetalert2');
// import Swal from 'sweetalert2'

Vue.use(VueResource);
Vue.use(VueSweetalert2);

Vue.use(DataTable)
new Vue({
  el: '#app',
  render: h => h(App),
  router:Router
})
