<?php

namespace App\Http\Controllers;

use App\Models\employees;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = employees::withoutTrashed()->get();
        return response()->json(['allemployees' => $employees], 201);
    }
    public function store(Request $request)
    {
        // $validate =  FacadesValidator::make(
        //     $request->all(),
        //     [
        //         'name' => 'required|unique:branches'

        //     ]

        // );
        // if ($validate->fails()) {
        //     // dd($validate);
        //     alert::warning('Error', 'Duplicate Entry..!!!');
        //     return redirect()->route('branch.createForm');
        //     // return redirect()->route('activity.create')->withErrors($validate);
        // }

        $employees = new employees();

        $employees->title = $request->title;
        $employees->fname = $request->fname;
        $employees->lname = $request->lname;
        $employees->email = $request->email;
        if ($employees->save()) {
            return response()->json(['message' => $employees], 201);
        }
    }

    public function trash($id)
    {
        $employees = employees::findOrFail($id);
        if (!$employees) {
            return response()->json(['message' => 'Employee not found'], 404);
        }
        $employees->delete();
        return response()->json(['message' => 'Employee deleted successfully'], 201);
    }

    public function update(Request $request, $id)
    {
        $employees = employees::findOrFail($id);

        $employees->fname  = $request->fname;
        $employees->lname  = $request->lname;
        $employees->email  = $request->email;
        $employees->title  = $request->title;


        $employees->save();
        return response()->json(['message' => 'Employee updated successfully'], 201);
    }
    public function edit($id)
    {
        $employees = employees::findOrFail($id);
        // dd($activity);
        return response()->json(['employee' => $employees], 201);
    }
}
