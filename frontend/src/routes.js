import Vue from 'vue'
import VueRouter from 'vue-router'
import employee_create from './components/employee/employee_create'
import employee_list from './components/employee/employee_list'
import employee_edit from './components/employee/employee_edit'
import login from './components/auth/login'
import register from './components/auth/register'

Vue.use(VueRouter);

const router = new VueRouter({

    mode:'history',

    routes:[
        {path:"",component:login},
        {path:"/login",component:login},
        {path:"/register",component:register},

        {path:"/employee_create",component:employee_create},
        {path:"/employee_list",component:employee_list},
        {path:"/employee_edit/:id",component:employee_edit}
    ]
});

export default router;